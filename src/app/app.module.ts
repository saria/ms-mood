import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatListModule, MatGridListModule, MatCardModule, MatButtonModule, 
         MatDialogModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, 
         MatSelectModule, MatSlideToggleModule, MatProgressSpinnerModule,
         MatSliderModule } from '@angular/material'; //*
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { WomanSpaceComponent } from './womanSpace/womanSpace.component';
import { ManSpaceComponent } from './manSpace/manSpace.component';
import { PieceComponent } from './piecedetail/piecedetail.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

import { PieceService } from './services/piece.service';
import { PromotionService } from './services/promotion.service';
import { LeaderService } from './services/leader.service';
import { FeedbackService } from './services/feedback.service';
import { ProcessHTTPMsgService } from './services/process-httpmsg.service';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginComponent } from './login/login.component';
import { baseURL } from './shared/baseurl';
import { HighlightDirective } from './directives/highlight.directive';


@NgModule({
  declarations: [
    AppComponent,
    WomanSpaceComponent,
    ManSpaceComponent,
    PieceComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatGridListModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule, 
    MatInputModule, 
    MatCheckboxModule,
    MatSelectModule, 
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatSliderModule, //*
    FlexLayoutModule,
    AppRoutingModule
  ],
  providers: [PieceService, PromotionService, LeaderService, ProcessHTTPMsgService,
  { provide: 'BaseURL', useValue: baseURL}],
  entryComponents: [LoginComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }

