import { Injectable } from '@angular/core';
import { Piece } from '../shared/piece';
import { Observable, of } from 'rxjs';
import { map, delay, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root' 
})
export class PieceService {

  constructor(private http: HttpClient, private processHTTPMsgService: ProcessHTTPMsgService) { }

  getPieces(): Observable<Piece[]> {
    return this.http.get<Piece[]>(baseURL + 'pieces')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFemalePieces(): Observable<Piece[]> {
    return this.http.get<Piece[]>(baseURL + 'pieces?gender=Female')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getMalePieces(): Observable<Piece[]> {
    return this.http.get<Piece[]>(baseURL + 'pieces?gender=Male')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getPiece(id: string): Observable<Piece> {
    return this.http.get<Piece>(baseURL + 'pieces/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFemaleFeaturedPiece(): Observable<Piece> {
    return this.http.get<Piece[]>(baseURL + 'pieces?gender=Female').pipe(map(pieces => pieces[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getMaleFeaturedPiece(): Observable<Piece> {
    return this.http.get<Piece[]>(baseURL + 'pieces?gender=Male').pipe(map(pieces => pieces[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeaturedPiece(): Observable<Piece> {
    return this.http.get<Piece[]>(baseURL + 'pieces?featured=true').pipe(map(pieces => pieces[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getPieceIds(): Observable<string[] | any> {
    return this.getPieces().pipe(map(pieces => pieces.map(piece => piece.id)))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFemalePieceIds(): Observable<string[] | any> {
    return this.getFemalePieces().pipe(map(pieces => pieces.map(piece => piece.id)))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getMalePieceIds(): Observable<string[] | any> {
    return this.getMalePieces().pipe(map(pieces => pieces.map(piece => piece.id)))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  putPiece(piece: Piece): Observable<Piece>
  {
      const httpOptions = 
      {
          headers: new HttpHeaders
          ({
             'Content-Type': 'application/json'
          })
      };

      return this.http.put<Piece>(baseURL + 'pieces/' + piece.id, piece, httpOptions)
                 .pipe(catchError(this.processHTTPMsgService.handleError));
  }  

}
