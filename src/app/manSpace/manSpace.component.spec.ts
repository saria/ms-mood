import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManSpaceComponent } from './manSpace.component';

describe('ManSpaceComponent', () => {
  let component: ManSpaceComponent;
  let fixture: ComponentFixture<ManSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManSpaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
