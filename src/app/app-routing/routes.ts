import { Routes } from '@angular/router';

import { WomanSpaceComponent } from '../womanSpace/womanSpace.component';
import { ManSpaceComponent } from '../manSpace/manSpace.component';
import { PieceComponent } from '../piecedetail/piecedetail.component';
import { HomeComponent } from '../home/home.component';
import { AboutComponent } from '../about/about.component';
import { ContactComponent } from '../contact/contact.component';

export const routes: Routes = [
   { path: 'home', component: HomeComponent },
   { path: 'womanSpace', component: WomanSpaceComponent },
   { path: 'manSpace', component: ManSpaceComponent },
   { path: 'piecedetail/:id', component: PieceComponent },
   { path: 'aboutus', component: AboutComponent },
   { path: 'contactus', component: ContactComponent },
   { path: '', redirectTo: '/home', pathMatch: 'full' }
];