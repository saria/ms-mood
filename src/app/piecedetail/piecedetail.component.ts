import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core'; // ViewChild
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; //*

import { Piece } from '../shared/piece';
import { PieceService} from '../services/piece.service';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../shared/comment'; // *
import { trigger, state, style, animate, transition } from '@angular/animations';

import { visibility, flyInOut, expand } from '../animations/app.animation';


@Component({
  selector: 'app-piecedetail',
  templateUrl: './piecedetail.component.html',
  styleUrls: ['./piecedetail.component.scss'],
  host: {
      '[@flyInOut]': 'true',
      'style': 'display: block;'
  },
  animations: [
      visibility(),
      flyInOut(),
      expand()
  ]
})
export class PieceComponent implements OnInit
{

  //@Input()
  piece : Piece;
  pieceIds: string[];
  prev: string;
  next: string;

  commentForm: FormGroup; //*
  comment: Comment; //*
  actualComment: Comment;
  @ViewChild('cform') commentFormDirective; //*

  errMess: string;
  piececopy: Piece;
  visibility = 'shown';

//*
formErrors = 
{
     'author': '',
     'rating': '',
     'comment': ''
}

//*
validationMessages = {
      'author': {
        'required': 'Author Name is required.',
        'minlength': 'Author Name must be at lest 2 characters long.',
        'maxlength': 'Author cannot be more than 25 characters long.'
      },
      'comment': {
        'required': 'Comment is required.'
      }
  }; 

  constructor( private dishService: PieceService, 
               private route: ActivatedRoute,
               private location: Location,
               private fb: FormBuilder,
               @Inject('BaseURL') private BaseURL) //*
  {
      this.createForm(); //* 
  }

  ngOnInit()
  {
      this.dishService.getFemalePieceIds().subscribe(pieceIds => this.pieceIds = pieceIds);

        this.route.params.pipe(switchMap((params: Params) => { this.visibility = 'hidden'; 
        return this.dishService.getPiece(params['id']) ; }))
    .subscribe(piece => { this.piece = piece; this.piececopy = piece; this.setPrevNext(piece.id); 
      this.visibility = 'shown'; },
      errmess => this.errMess = <any>errmess);

  }

  setPrevNext(pieceId: string)
  {
      const index = this.pieceIds.indexOf(pieceId);
      this.prev = this.pieceIds[(this.pieceIds.length + index - 1) % this.pieceIds.length];
      this.next = this.pieceIds[(this.pieceIds.length + index + 1) % this.pieceIds.length];
  }

  goBack(): void
  {
     this.location.back();
  }

  //*
  createForm()
  {
      this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: 5,
      comment: ['', [Validators.required]]
    });
      this.commentForm.valueChanges.subscribe(data => this.onValueChanged(data));
      this.onValueChanged(); // reset form validation messages
  }

  //*
  onValueChanged(data?: any)
  {
      if(!this.commentForm)
      {
        return;
      }
      const form = this.commentForm;

      this.actualComment = this.commentForm.value;
      
      for (const field in this.formErrors)
      {
          if(this.formErrors.hasOwnProperty(field))
          {
              // clear prevuous error messages (if any)
              this.formErrors[field] = '';
              const control = form.get(field);
              if(control && control.dirty && !control.valid)
              {
                const messages = this.validationMessages[field];
                for(const key in control.errors )
                {
                    if (control.errors.hasOwnProperty(key))
                    {
                        this.formErrors[field] += messages[key] + ' ';
                    }
                }
              }
          }
      }
  }

  //*
  onSubmit()
  {
      this.comment = this.commentForm.value;
      console.log(this.comment);

      var d = new Date();
      var n = d.toISOString();
      this.actualComment = this.commentForm.value;
      this.actualComment.date = n;
      console.log('SUBMIT ------------- 1: ' + this.actualComment.author);
      this.piececopy.comments.push(this.actualComment);
      this.dishService.putPiece(this.piececopy).subscribe(piece => {
        this.piece = piece; 
        this.piececopy = piece;
      },
      errmess => {
        this.piece = null; 
        this.piececopy = null; 
        this.errMess = <any> errmess;
      });
      console.log('SUBMIT ------------- Size: ' + this.piece.comments.length);

      this.commentForm.reset({
        author: '',
        rating: 5,
        comment: ''
      });
      /*this.commentFormDirective.resetForm();*/
  }

 /* updateSetting(event)
  {
      this.rating = event.value;
      this.commentForm.controls.rating.value = this.rating;
  }*/

}
