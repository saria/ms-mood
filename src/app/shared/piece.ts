import { Comment } from './comment';
import { Color } from './color';
import { Occasion } from './occasion';
import { Material } from './material';

export class Piece
{
	id: string;
	label: string;
	image: string;
	featured: boolean;
	price: string;
	description: string;
	size: string;
	gender: string;
	colors: Color[];
	occasions: Occasion[];
	materials: Material[];
	comments: Comment[];
}