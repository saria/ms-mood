import { Component, OnInit, Inject } from '@angular/core';

import { Piece } from '../shared/piece';
import { PieceService } from '../services/piece.service';
import { Promotion } from '../shared/promotion';
import { PromotionService } from '../services/promotion.service';
import { Leader } from '../shared/leader';
import { LeaderService } from '../services/leader.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
      '[@flyInOut]': 'true',
      'style': 'display: block;'
  },
  animations: [
      flyInOut(),
      expand()
  ]
})
export class HomeComponent implements OnInit {

  femaleClo: Piece;
  maleClo: Piece;
  promotion: Promotion;
  leader: Leader;
  femclErrMess: string;
  malclErrMess: string;

  constructor(private dishService: PieceService, private promotionService: PromotionService, 
    private leaderService: LeaderService,
    @Inject('BaseURL') private BaseURL) { }


  ngOnInit()
  {
  	/*this.dishService.getFeaturedPiece().then(piece => this.piece = piece);
  	this.promotionService.getFeaturedPromotion().then(promotion => this.promotion = promotion);
    this.leaderService.getFeaturedLeader().then(leader => this.leader = leader);*/

    this.dishService.getFemaleFeaturedPiece()
        .subscribe(femaleClo => this.femaleClo = femaleClo ,
                   errmess => this.femclErrMess = <any> errmess);
    this.dishService.getMaleFeaturedPiece()
        .subscribe(maleClo => this.maleClo = maleClo ,
                   errmess => this.malclErrMess = <any> errmess);
  }

}


