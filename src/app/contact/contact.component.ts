import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { FeedbackService } from '../services/feedback.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { visibility, submitVisibility, feedFormVisibility, flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  preserveWhitespaces: false,
  host: {
      '[@flyInOut]': 'true',
      'style': 'display: block;'
  },
  animations: [
      flyInOut(),
      visibility(),
      submitVisibility(),
      feedFormVisibility(),
      expand()
  ]
})
export class ContactComponent implements OnInit {

  errMess: string;
  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType = ContactType; 
  @ViewChild('fform') feedbackFormDirective;
  visibility = 'shown';
  submitVisibility = 'shown';
  feedFormVisibility = 'hide';
  showSpinner: string;
  hideSpinner: string;
  lat: number;
  lng: number;

  formErrors = {
     'firstname': '',
     'lastname': '',
     'telnum': '',
     'email': ''
  }

  validationMessages = {
      'firstname': {
        'required': 'First name is required.',
        'minlength': 'First name must be at lest 2 characters long.',
        'maxlength': 'First name cannot be more than 25 characters long.'
      },
      'lastname': {
        'required': 'Last name is required.',
        'minlength': 'Last name must be at lest 2 characters long.',
        'maxlength': 'Last name cannot be more than 25 characters long.'
      },
      'telnum': {
        'required': 'Tel. Number is required.',
        'pattern': 'Tel. Number must contain only numbers.'
      },
      'email': {
        'required': 'Email is required.',
        'email': 'Email not invalid format.'
      }
  }; 

  constructor( private feedbackService: FeedbackService, 
               private fb: FormBuilder,
               @Inject('BaseURL') private BaseURL) 
  {
     this.createForm();
  }

  ngOnInit()
  {
    this.lat = 35.85427442288666;
    this.lng = 9.210845232009888;
  }

  createForm()
  {
      this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]], 
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: [0, [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });
      this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));
      this.onValueChanged(); // reset form validation messages
  }

  onValueChanged(data?: any)
  {
      if(!this.feedbackForm)
      {
        return;
      }
      const form = this.feedbackForm;
      for (const field in this.formErrors)
      {
          if(this.formErrors.hasOwnProperty(field))
          {
              // clear prevuous error messages (if any)
              this.formErrors[field] = '';
              const control = form.get(field);
              if(control && control.dirty && !control.valid)
              {
                const messages = this.validationMessages[field];
                for(const key in control.errors )
                {
                    if (control.errors.hasOwnProperty(key))
                    {
                        this.formErrors[field] += messages[key] + ' ';
                    }
                }
              }
          }
      }
  }

  onSubmit()
  {

      this.feedback = this.feedbackForm.value;
      console.log(this.feedback);
      console.log('------------------- 1');

      this.feedbackService.postFeedback(this.feedback).subscribe(feedback =>
      {
          console.log('--> RESOLVED');
          this.feedback = feedback;
          this.hideSpinner = "yes";
          this.submitVisibility = 'hidden';

          setTimeout(() => 
          {
              this.visibility = 'shown';
              console.log('------------------- 3');
              this.feedbackForm.reset({
                  firstname: '',
                  lastname: '',
                  telnum: 0,
                  email: '',
                  agree: false,
                  contacttype: 'None',
                  message: ''
              });
              console.log('------------------- 4');
              this.feedbackFormDirective.resetForm();
          }, 3000);
      },
      errmess => 
      {
          console.log('--> REJECTED');
          this.feedback = null; 
          this.errMess = <any> errmess;
      });

      this.visibility = 'hidden';
      this.showSpinner = "yes";
      console.log('------------------- 2');

  }
}
