import { Component, OnInit, Inject } from '@angular/core';
import { Piece } from '../shared/piece';
import { PieceService } from '../services/piece.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-womanSpace',
  templateUrl: './womanSpace.component.html',
  styleUrls: ['./womanSpace.component.scss'],
  host: {
      '[@flyInOut]': 'true',
      'style': 'display: block;'
  },
  animations: [
      flyInOut(),
      expand()
  ]
})
export class WomanSpaceComponent implements OnInit
{
  pieces: Piece[];
  errMess: string;

  // selectedDish: Piece;
  
  constructor(private dishService: PieceService,
    @Inject('BaseURL') private BaseURL) { }

  ngOnInit()
  {
     // this.pieces = this.dishService.getPieces();
     // this.dishService.getPieces().then((pieces) => this.pieces = pieces)
     this.dishService.getFemalePieces().subscribe((pieces) => this.pieces = pieces, 
      errmess => this.errMess = <any> errmess);
  }

  /*onSelect(piece : Piece)
  {
    this.selectedDish = piece;
  }*/

}
