import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WomanSpaceComponent } from './womanSpace.component';

describe('WomanSpaceComponent', () => {
  let component: WomanSpaceComponent;
  let fixture: ComponentFixture<WomanSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WomanSpaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WomanSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
